sudo yum install yum-utils -y -q
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum install -y -q yum-utils jq terraform
touch /tmp/credentials
curl -H "Authorization: $AWS_CONTAINER_AUTHORIZATION_TOKEN" $AWS_CONTAINER_CREDENTIALS_FULL_URI 2>/dev/null > /tmp/credentials
ACCESS_KEY=`cat /tmp/credentials| jq -r .AccessKeyId`
SECRET_KEY=`cat /tmp/credentials| jq -r .SecretAccessKey`
SESSION_TOKEN=`cat /tmp/credentials| jq -r .Token`
ACCOUNTID=$(aws sts get-caller-identity --query "Account" --output text)
clear

aws configure set aws_access_key_id $ACCESS_KEY; aws configure set aws_secret_access_key $SECRET_KEY; aws configure set aws_session_token $SESSION_TOKEN ; aws configure set default.region us-east-1

#echo -ne "\n##### This is a temporary credential for the accountID $ACCOUNTID. ##### \n\n AWS_ACCESS_KEY_ID=${ACCESS_KEY}\n AWS_SECRET_ACCESS_KEY=${SECRET_KEY}\n AWS_SESSION_TOKEN=${SESSION_TOKEN}\n\n" | tee ~/.aws/credentials
