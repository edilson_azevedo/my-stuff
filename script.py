import boto3
import requests
import json
import csv
from datetime import datetime, timedelta

def count_resource_occurrences(group_id):
    client = boto3.client('cloudtrail')
    paginator = client.get_paginator('lookup_events')

    start_time = datetime.utcnow() - timedelta(days=30)
    end_time = datetime.utcnow()

    page_iterator = paginator.paginate(
        StartTime=start_time,
        EndTime=end_time,
        LookupAttributes=[{'AttributeKey': 'EventName', 'AttributeValue': 'ModifyNetworkInterfaceAttribute'}]
    )

    found_occurrence = False
    occurrence_count = 0
    event_time = ""

    for page in page_iterator:
        for event in page['Events']:
            resources = event.get('Resources', [])
            for resource in resources:
                event_resource_name = resource.get('ResourceName', '')
                if event_resource_name == group_id:
                    found_occurrence = True
                    occurrence_count += 1
                    event_time = event.get('EventTime', '')
            if found_occurrence:
                break
        if found_occurrence:
            break

    return found_occurrence, occurrence_count, event_time

def get_report_file():
    used_SG = set()
    unused_SG_not_default = set()
    total_all_regions_SG = set()
    account_id = boto3.client('sts').get_caller_identity().get('Account')
    regions = ['us-east-1', 'sa-east-1']

    for region in regions:
        ec2 = boto3.client("ec2", region_name=region)
        elb = boto3.client("elb", region_name=region)
        elbv2 = boto3.client("elbv2", region_name=region)
        rds = boto3.client("rds", region_name=region)
        eni = boto3.client("ec2", region_name=region)

        response = ec2.describe_instances()
        for reservation in response["Reservations"]:
            for instance in reservation["Instances"]:
                for sg in instance["SecurityGroups"]:
                    used_SG.add((region, sg["GroupId"], sg.get("GroupName", "")))

        response = elb.describe_load_balancers()
        for lb in response["LoadBalancerDescriptions"]:
            for sg in lb["SecurityGroups"]:
                used_SG.add((region, sg, ""))

        response = elbv2.describe_load_balancers()
        for lb in response["LoadBalancers"]:
            if lb.get('SecurityGroups'):
                for sg in lb["SecurityGroups"]:
                    used_SG.add((region, sg, ""))

        response = rds.describe_db_instances()
        for instance in response["DBInstances"]:
            for sg in instance["VpcSecurityGroups"]:
                used_SG.add((region, sg["VpcSecurityGroupId"], sg.get("VpcSecurityGroupName", "")))

        response = eni.describe_network_interfaces()
        for eni in response["NetworkInterfaces"]:
            for group in eni["Groups"]:
                used_SG.add((region, group["GroupId"], group.get("GroupName", "")))

        response = ec2.describe_security_groups()
        for sg in response["SecurityGroups"]:
            total_all_regions_SG.add((region, sg["GroupId"], sg.get("GroupName", "")))

    unused_SG = total_all_regions_SG - used_SG

    for item in unused_SG:
        unused_SG_not_default.add(item)

    for item in unused_SG:
        region, group_id, group_name = item  # Unpack the tuple into region, group_id, and group_name
        unused_SG_not_default.add((region, group_id, group_name))

    file = open('/tmp/result.html', 'w')
    file.write("<html><head>")
    file.write("<style>")
    file.write("table {border-collapse: collapse;}")
    file.write("th, td {border: 1px solid black; padding: 8px;}")
    file.write("th {background-color: #f2f2f2;}")
    file.write("</style>")
    file.write("</head><body>")
    file.write("<h1>Rule: UNUSED SECURITY GROUPS</h1>")
    file.write(f"<p>Account: {account_id}</p>")
    file.write(f"<p>Total Security Groups: {len(total_all_regions_SG)}</p>")
    file.write(f"<p>Used Security Groups: {len(used_SG)}</p>")
    file.write(f"<p>Unused Security Groups (non-default): {len(unused_SG_not_default)}</p>")
    file.write("<table>")
    file.write("<tr><th>REGION</th><th>GROUP ID</th><th>GROUP NAME</th><th>LAST 30D USAGE</th><th>EVENT TIME</th></tr>")

    for item in unused_SG_not_default:
        region, group_id, group_name = item
        found_occurrence, occurrence_count, event_time = count_resource_occurrences(group_id)
        occurrence_result = "yes" if found_occurrence else "no"
        file.write("<tr>")
        file.write(f"<td>{region}</td>")
        file.write(f"<td>{group_id}</td>")
        file.write(f"<td>{group_name}</td>")
        file.write(f"<td>{occurrence_result}</td>")
        
        if found_occurrence:
            file.write(f"<td>{event_time}</td>")
        else:
            file.write("<td>N/A</td>")
        
        file.write("</tr>")

    file.write("</table>")
    file.write("</body></html>")
    file.close()


    # Create the CSV file
    csv_file_path = '/tmp/result.csv'
    with open(csv_file_path, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(['REGION', 'GROUP ID', 'GROUP NAME', 'OCCURRENCE', 'EVENT TIME'])

        for item in unused_SG_not_default:
            region, group_id, group_name = item
            found_occurrence, occurrence_count, event_time = count_resource_occurrences(group_id)
            occurrence_result = "yes" if found_occurrence else "no"
            writer.writerow([region, group_id, group_name, f"{occurrence_result} ({occurrence_count})", event_time])

    # Upload the CSV file to S3
    s3_client = boto3.client('s3')
    bucket_name = 'agibank-sg-report-source'
    s3_key = 'outputs_reports/result.csv'
    s3_client.upload_file(csv_file_path, bucket_name, s3_key)

def send_mail():
    ses_client = boto3.client("ses", region_name='us-east-1')
    CHARSET = "UTF-8"

    with open('/tmp/result.html', 'rt') as file:
        read_content = file.read()

    response = ses_client.send_email(
        Destination={
            "ToAddresses": [
                "ntconsult.edilson@agi.com.br",
                "edilson.azevedo@ntconsultcorp.com",
            ],
        },
        Message={
            "Body": {
                "Html": {
                    "Charset": CHARSET,
                    "Data": read_content,
                }
            },
            "Subject": {
                "Charset": CHARSET,
                "Data": "SG REPORT. RULE: UNUSED SECURITY GROUPS",
            },
        },
        Source="noreply@agi.com.br",
    )

def send_qualitor():
    file = open('/tmp/result.txt', 'rt')
    read_content = file.read()

    url = "https://api.agibank.com.br/qualitor-integrator/external/qualitor/addticket"

    payload = json.dumps({
        "codigoCategoria": "189009853",
        "codigoCliente": "1",
        "codigoContato": "26523",
        "codigoLocalidade": "4089",
        "codigoSeveridade": "2",
        "codigoTipoChamado": "40",
        "descricao": read_content,
        "nomeFuncionario": "svc.botinfra.prd",
        "titulo": "SG Report - unused SG all regions"
    })
    headers = {
        'Content-Type': 'application/json',
        'api-key': 'FEBnkfjlh@veeuFBHE'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

def lambda_handler(event, context):
    get_report_file()
    send_mail()
    # send_qualitor()