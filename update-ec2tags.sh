#!/bin/bash

# AWS CLI profile
AWS_PROFILE="your_aws_profile"

# AWS CLI command to list instances with the specified tag
instances=$(aws ec2 describe-instances --profile $AWS_PROFILE --filters "Name=tag-key,Values=SSM" "Name=tag-value,Values=YES" --query "Reservations[].Instances[].InstanceId" --output text)

# Check if any instances are found
if [ -z "$instances" ]; then
    echo "No instances found with the tag key 'SSM' and value 'YES'. Exiting."
    exit 1
fi

# Display a summary of affected instances
echo "Found the following instances with the tag key 'SSM' and value 'YES':"
aws ec2 describe-instances --profile $AWS_PROFILE --instance-ids $instances --query "Reservations[].Instances[].{InstanceId:InstanceId, Tags:Tags}" --output table

# Count the number of affected instances
total_instances=$(echo "$instances" | wc -w)
echo "Total instances affected: $total_instances"

# Ask for confirmation before proceeding
read -p "Do you want to proceed and update the tags? (yes/no): " confirm

if [ "$confirm" != "yes" ]; then
    echo "Operation cancelled. No changes made."
    exit 0
fi

# Update the tags for each instance
for instance_id in $instances; do
    aws ec2 create-tags --profile $AWS_PROFILE --resources $instance_id --tags "Key=SSM,Value=SIM"
    echo "Updated tags for instance $instance_id"
done

echo "Tag update completed successfully."
